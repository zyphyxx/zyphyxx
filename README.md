[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/edgar-smj/)
[![Gmail](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white
)](mailto:mendesbr92@gmail.com)
[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/zyphyxx)

<div align="center"> 
 <h2> 👋 Sobre mim</h2>
</div>
Olá, meu nome é Edgar sou de Suzano - SP. Estudante em Gestão da Tecnologia da Informação. Durante anos, adquiri habilidades como organização, resolução de problemas e trabalho em equipe, que acredito serem fundamentais para minha jornada na área de desenvolvimento. Estou entusiasmado para aplicar minha experiência anterior e minha paixão pela tecnologia para enfrentar novos desafios e aprender continuamente nesta nova área.

<div align="center"> 
 <h2> 🛠️ Habilidades e Tecnologias</h2>
 
 [![My Skills](https://skillicons.dev/icons?i=java,kotlin,spring,angular,idea,aws,kafka,docker,maven,mysql,postman)](https://skillicons.dev)
</div>

Aqui você encontrará alguns projetos focados nas seguintes tecnologias que estou aprendendo.
- Java
- Kotlin
- Spring Boot (Web, Data JPA, Security)
- Microservices
- Relational and Non-Relational Databases (PostgreSQL, MySQL, MongoDB)
- Docker
- Kafka
- AWS

## 
<!-- GRS (Light Mode) -->
<div align="center"> 
  <a href="https://github.com/zyphyxx#gh-light-mode-only">
    <img
      src="https://github-readme-stats-steel-omega.vercel.app/api?username=zyphyxx&show_icons=true&include_all_commits=true&hide_border=true&number_format=long&rank_icon=percentile&show=reviews,discussions_started,discussions_answered,prs_merged,prs_merged_percentage#gh-light-mode-only"
      alt="My Github stats"
      height="370"
    />
  </a>
  <a href="https://github.com/zyphyxx#gh-light-mode-only">
    <img
      src="https://github-readme-stats-steel-omega.vercel.app/api/top-langs/?username=zyphyxx&layout=pie&hide_border=true&langs_count=10#gh-light-mode-only"
      alt="My Language stats"
      height="370"
    />
  </a>
</div>

<!-- GRS (Dark Mode) -->
<div align="center"> 
  <a href="https://github.com/zyphyxx#gh-dark-mode-only">
    <img
      src="https://github-readme-stats-steel-omega.vercel.app/api?username=zyphyxx&show_icons=true&include_all_commits=true&icon_color=2d77dc&title_color=2d77dc&text_color=ffffff&bg_color=0d1117&hide_border=true&number_format=long&rank_icon=percentile&show=reviews,discussions_started,discussions_answered,prs_merged,prs_merged_percentage#gh-dark-mode-only"
      alt="My Github stats"
      height="370"
    />
  </a>
  <a href="https://github.com/zyphyxx#gh-dark-mode-only">
    <img
      src="https://github-readme-stats-steel-omega.vercel.app/api/top-langs/?username=zyphyxx&layout=pie&icon_color=2d77dc&title_color=2d77dc&text_color=ffffff&bg_color=0d1117&hide_border=true&langs_count=10#gh-dark-mode-only"
      alt="My Language stats"
      height="370"
    />
  </a>
</div>

<!-- Streal stats (Light mode) -->
<div align="center">
  <a href="https://github.com/zyphyxx#gh-light-mode-only">
    <img
       src="https://github-readme-streak-stats-phi-opal.vercel.app/?user=zyphyxx&locale=en&type=svg&hide_border=true&fire=2d77dc&ring=2d77dc&currStreakLabel=000000"
       alt="My GitHub streak stats"
     />
  </a>
</div>


<!-- Streal stats (Dark mode) -->
<div align="center">
  <a href="https://github.com/zyphyxx#gh-dark-mode-only">
    <img
       src="https://github-readme-streak-stats-phi-opal.vercel.app/?user=zyphyxx&background=0d1117&currStreakNum=ffffff&sideNums=ffffff&currStreakLabel=ffffff&sideLabels=ffffff&dates=ffffff&fire=2d77dc&ring=2d77dc&locale=en&type=svg&hide_border=true"
       alt="My GitHub streak stats"
     />
  </a>
</div>
